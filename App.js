/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import List from './List';

const App: () => React$Node = () => {
  return <List />;
};

export default App;
