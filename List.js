import React, {PureComponent} from 'react';
import {StyleSheet, View, Text, FlatList, Image} from 'react-native';

class List extends PureComponent {
  state = {listData: []};

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    try {
      const res = await fetch(
        'https://randomuser.me/api/?inc=picture,name&results=30',
      );
      const json = await res.json();
      this.setState({listData: json.results});
    } catch (err) {
      console.log(err);
    }
  };

  renderItem = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <Image source={{uri: item.picture.large}} style={styles.avatar} />
        <Text style={styles.name}>
          {[item.name.title, item.name.first, item.name.last].join(' ')}
        </Text>
      </View>
    );
  };

  render() {
    return (
      <View>
        <FlatList data={this.state.listData} renderItem={this.renderItem} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#ddd',
    marginRight: 20,
  },
  name: {
    fontSize: 20,
  },
});

List.propTypes = {};

export default List;
